import random
from turingarena import *

def reset(matrix):
	global field_size
	field_size = 10
	for i in range(len(matrix)):
		matrix.pop(i)
	
	for i in range(field_size):
		row = [0 for j in range(field_size)]
		matrix.append(row)
		

def print_grid(grid):
	for i in range(field_size):
		print("{:02d}".format(field_size - i), end=' ')
		for j in range(field_size):
			print(board_pieces[grid[i][j]], end=' ')
		print()

	print("   ", end ='')
	for i in range(field_size):
		print(i+1, end=' ')

	print()

board_pieces = {
	-1:"±",
	0:"∼",
	1:"#",
	2:"/",
}

n_duels = 3
for duel in range(n_duels):
	fields = ([], [])		
	for grid in fields:
		reset(grid)			

	with run_algorithm(submission.player1) as p1, run_algorithm(submission.player2) as p2:
		players = (p1, p2)

		def place_ship(ship_len, x, y, is_horizontal):
			grid = fields[current_player]

			if not is_a_suitable_place(ship_len, x, y, is_horizontal):
				return 1

			for i in range(ship_len):
				if is_horizontal == 1:
					grid[field_size-y][x-1+i] = 1
				else:
					grid[field_size-y-i][x-1] = 1

			return 0

		def is_a_suitable_place(ship_len, x, y, is_horizontal):

			grid = fields[current_player]
			try:
				for i in range(ship_len):
					if is_horizontal:
						if not grid[field_size-y][x-1+i] == 0:
							return False
					elif not grid[field_size-y-i][x-1] == 0:
						return False

				return True

			except IndexError:
				return False


		def guess(x, y):
			enemy_grid = fields[(current_player+1)%2]
			
			if enemy_grid[field_size-y][x-1] == -1 or enemy_grid[field_size-y][x-1] == 2:
				return -1

			plays[current_player] += 1

			if enemy_grid[field_size-y][x-1] == 0:
				enemy_grid[field_size-y][x-1] = -1
				return 0

			else:
				enemy_grid[field_size-y][x-1] = 2
				is_horizontal = is_placed_horizontally(enemy_grid, x, y)
	
			i = 0
			while is_horizontal and (x-1-i) < 0 or (field_size-y-i) < 0:

				if is_horizontal and enemy_grid[field_size-y][x-1-i] > 0 or enemy_grid[field_size-y-i][x-1] > 0:
					i += 1
				else:
					break

			while not is_horizontal and enemy_grid[field_size-y][x-1-i] < 0 or is_horizontal and enemy_grid[field_size-y-i][x-1] < 0:

				if is_horizontal and not enemy_grid[field_size-y][x-1-i] == 2 or not enemy_grid[field_size-y-i][x-1] == 2:
					return 1
				else:
					i -= 1

			return 2

		def is_placed_horizontally(grid, x, y):
			rel_coords = [[0, -1],[-1, 0], [1, 0], [0, 1]]

			for coord in rel_coords:
				try:
					if grid[field_size-y+coord[1]][x-1+coord[0]] > 0:
						return not coord[0] == 0
						break
				except IndexError:
					pass

		global plays
		plays = [0, 0]
		first_player = duel%2
		print(f"Player {first_player+1} goes first!")

		for i, p in enumerate(players):
			global current_player
			current_player = i
			p.procedures.start(callbacks=[place_ship])

		for i, p in enumerate(players):
			current_player = i
			p.procedures.play(callbacks=[guess])

		
		for i, grid in enumerate(fields):
			print(f"\nPlayer {i+1}'s grid:")
			print_grid(grid)

		if plays[first_player] < plays[(first_player-1)%2]:
			print(f"Player {first_player+1} sank all of Player {(first_player-1)%2+1}'s ships first!")
		else:
			print(f"Player {(first_player-1)%2+1} sank all of Player {first_player+1}'s ships first!")
