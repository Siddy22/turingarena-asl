import random
from turingarena import *

def reset(matrix):
	global field_size
	field_size = 10
	for i in range(len(matrix)):
		matrix.pop(i)
	
	for i in range(field_size):
		row = [0 for j in range(field_size)]
		matrix.append(row)
		

def print_grid(grid):
	for i in range(field_size):
		print("{:02d}".format(field_size - i), end=' ')
		for j in range(field_size):
			print(board_pieces[grid[i][j]], end=' ')
		print()

	print("   ", end ='')
	for i in range(field_size):
		print(i+1, end=' ')

	print()

board_pieces = {
	-1:"±",
	0:"∼",
	1:"#",
	2:"/",
}

n_duels = 1
with run_algorithm(submission.player1) as p1, run_algorithm(submission.player2) as p2:
	players = (p1, p2)

	for p in players:
		p.procedures.start(n_duels)

	for duel in range(n_duels):
		fields = ([], [])
		ships = ([], [])
		for grid in fields:	
			reset(grid)		

		def place(ship_len, x, y, is_horizontal):
			grid = fields[current_player]
			ships_placed = ships[current_player]

			if not is_a_suitable_place(ship_len, x, y, is_horizontal):
				return 1

			for i in range(ship_len):
				if is_horizontal == 1:
					grid[field_size-y][x-1+i] = 1
				else:
					grid[field_size-y-i][x-1] = 1

			return 0

		def is_a_suitable_place(ship_len, x, y, is_horizontal):

			rel_coords = [[0, 1], [-1, 0], [1, 0], [0, -1]]
			grid = fields[current_player]
			try:
				for i in range(ship_len):
					ship_coords = [x-1, field_size-y]
					if is_horizontal:
						ship_coords[0] += i
					else:
						ship_coords[1] -= i

					if not grid[field_size-y][x-1+i] == 0:
						return False
					elif not grid[field_size-y][x-1+i] == 0:
						return False

					try:
						for coords in rel_coords:
							if not grid[ship_coords[1]+coords[1]][ship_coords[0]+coords[0]] == 0:
								return False
					except IndexError:
						pass

				return True

			except IndexError:
				return False


		def guess(x, y):
			enemy_grid = fields[(current_player+1)%2]

			if enemy_grid[field_size-y][x-1] == -1 or enemy_grid[field_size-y][x-1] == 2:
				return -1

			plays[current_player] += 1

			if enemy_grid[field_size-y][x-1] == 0:
				enemy_grid[field_size-y][x-1] = -1
				return 0

			else:
				enemy_grid[field_size-y][x-1] = 2
				is_horizontal = is_placed_horizontally(enemy_grid, x, y)
	
			direction = 1
			offset = direction
			found_ship_square = False
			
			'''while is_horizontal and is_in_bounds(x-1-offset) or not is_horizontal and is_in_bounds(field_size-y-offset):
				
				if is_horizontal and enemy_grid[field_size-y][x-1-offset] > 0 or not is_horizontal and enemy_grid[field_size-y-offset][x-1] > 0:
					print(field_size-y, x-1-offset)
					offset += direction
					found_square_ship = True
				else:
					if not found_ship_square:
						direction = -1
						offset = 0
						found_ship_square = True
					else:
						break'''

			while True:
				if (is_horizontal and is_in_bounds(x-1-offset) and enemy_grid[field_size-y][x-1-offset] > 0) or (not is_horizontal and is_in_bounds(field_size-y-offset) and enemy_grid[field_size-y-offset][x-1] > 0):
					offset += direction
					found_ship_square = True
				else:
					if not found_ship_square:
						direction *= -1
						offset = direction
						found_ship_square = True
					else:
						offset -= direction
						break


			while True:
				if current_player == 0:
					print_grid(enemy_grid)
					print(x, y)
				if is_horizontal:
					if enemy_grid[field_size-y][x-1-offset] <= 0:
						break;
				else:
					if enemy_grid[field_size-y-offset][x-1] <= 0:
						break;

				if is_horizontal and not enemy_grid[field_size-y][x-1-offset] == 2 or not is_horizontal and not enemy_grid[field_size-y-i][x-offset] == 2:
					print_grid(enemy_grid)
					return 1
				else:
					offset -= direction

			return 2

		def is_in_bounds(num):
			return num >= 0 and num < field_size

		def printt(n):
			print(current_player, n)

		def is_placed_horizontally(grid, x, y):
			rel_coords = [[0, -1],[-1, 0], [1, 0], [0, 1]]

			for coord in rel_coords:
				try:
					if grid[field_size-y+coord[1]][x-1+coord[0]] > 0:
						return not coord[0] == 0
						break
				except IndexError:
					pass

		global plays
		plays = [0, 0]
		first_player = duel%2
		print(f"Player {first_player+1} goes first!")

		for i, p in enumerate(players):
			global current_player
			current_player = i
			p.procedures.place_all_ships(callbacks=[place])

		for i, p in enumerate(players):
			current_player = i
			p.procedures.play(callbacks=[guess, printt])

		
		for i, grid in enumerate(fields):
			print(f"\nPlayer {i+1}'s grid:")
			print_grid(grid)

		if plays[first_player] < plays[(first_player-1)%2]:
			print(f"Player {first_player+1} sank all of Player {(first_player-1)%2+1}'s ships first!")
		else:
			print(f"Player {(first_player-1)%2+1} sank all of Player {first_player+1}'s ships first!")
