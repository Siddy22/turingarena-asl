#include <stdlib.h>

int total_ship_len = 10;

void place_all_ships(int place(int ship_len, int x, int y, int is_horizontal)) {
	for(int i = 1; i <= 4; i++) {
		for(int j = 0; j < 4-i+1; j++) {
			int placed_correctly;

			do {
				placed_correctly = place(i, rand()%10+1, rand()%10+1, rand()%2);
				if(placed_correctly == 0) total_ship_len += i;
			} while(placed_correctly != 0);

		}
	}
}

void start(int n) {}

void play(int guess(int x, int y)) {

	for(int i = 1; i <= 10; i++) {
		for(int j = 1; j <= 10; j++)
			guess(j, i);
	}

}
