#include <stdlib.h>
#include <chrono>

int total_ship_len;

void start(int n) {srand(1301);}

void place_all_ships(int place(int ship_len, int x, int y, int is_horizontal)) {

	total_ship_len = 0;

	for(int i = 1; i <= 4; i++) {
		for(int j = 0; j < 4-i+1; j++) {
			int placed_correctly;

			do {
				placed_correctly = place(i, rand()%10+1, rand()%10+1, rand()%2);
				if(placed_correctly == 0) total_ship_len += i;
			} while(placed_correctly != 0);

		}
	}

}

bool is_in_bounds(int number) { // 10 >= n >= 1
	return 10 >= number && number >= 1;
}

void copy_array(int *from, int *to, int len) {
	for (int i = 0; i < len; i++)
		to[i] = from[i];
}

void play(int guess(int x, int y)) {
	int hits = 0;
	bool is_horizontal;

	while(hits < total_ship_len) {	

		int coords[2] = {rand()%10+1, rand()%10+1};
		switch(guess(coords[0], coords[1])) {

			case -1: case 0: break;
	
			case 1:
			{
				hits++;
				bool sank = false;

				int ship_start[2] = {coords[0], coords[1]};

				int relative_coords[4][2] = {{0, 1}, {-1, 0}, {1, 0}, {0, -1}};
				int direction;
				for(int i = 0; i < 4; i++) {
					if(is_in_bounds(ship_start[0]+relative_coords[i][0]) && is_in_bounds(ship_start[1]+relative_coords[i][1])) {
						int results = guess(ship_start[0]+relative_coords[i][0], ship_start[1]+relative_coords[i][1]);
						if(results > 0) {
							hits++;
							if(results == 2) sank = true;
							is_horizontal = relative_coords[i][0] != 0;
							direction = relative_coords[i][0] + relative_coords[i][1];
							break;
						}
					}
				}

				int offset = direction*2;
				while(!sank) {
					if(!is_in_bounds(ship_start[0]+offset) && is_horizontal || !is_in_bounds(ship_start[1]+offset) && !is_horizontal) {
						direction *= -1;
						offset = direction;
					}

					if(is_horizontal) copy_array(new int[2]{ship_start[0]+offset, ship_start[1]}, coords, 2);
					else copy_array(new int[2]{ship_start[0], ship_start[1]+offset}, coords, 2);

					switch(guess(coords[0], coords[1])) {

						case -1: case 0: direction *= -1; offset = direction; break;

						case 1: hits++; offset += direction; break;

						case 2: hits++; sank = true;
	
					}

				}

				break;
			}

			case 2: hits++;

		}
	}

}
