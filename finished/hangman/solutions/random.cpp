
bool is_word_complete(int length, int *word) {

	for(int i = 0; i < length; i++) {
		if(word[i] == -1) return false;
	}

	return true;

}

#include <stdlib.h>
void play(int length, void guess(int letter), int get_word_letter(int i)) {
	
	int *word = new int[length];
	for(int i = 0; i < length; i++)
		word[i] = -1;

	while(!is_word_complete(length, word)) {
		int letter = rand() % 26 + 97;

		guess(letter);

		for(int i = 0; i < length; i++)
			word[i] = get_word_letter(i);
	}

}
