
void play(int length, void guess(int letter), int get_word_letter(int i)) {

	int order[] = {101, 116, 97, 105, 110, 111, 115, 104, 114, 100, 108, 117, 99, 109, 102, 119, 121, 103, 112, 98, 118, 107, 113, 106, 120, 122};
	/* I numeri sono le lettere che appaiono di più nella lingua inglese, ovvero
	 * {"e", "t", "a", "i", "n", "o", "s", "h", "r", "d", "l", "u", "c", "m", "f", "w", "y", "g", "p", "b", "v", "k", "q", "j", "x", "z"}
	 */

	for(int i = 0; i < 26; i++) {

		guess(order[i]);
		
		for(int j = 0; j < length; j++) {
			if(get_word_letter(j) == -1) break;
			if(j == length-1) return;
		}
	
	}

}
