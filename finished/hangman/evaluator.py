import random
from turingarena import *

def main():
	less_than_eleven_tries = True
	guessed_each_letter_only_once = True
	
	for _ in range(10):
		word = turn_into_int_array(get_random_word("dictionary.txt"))
		has_found_letter = fill_boolean_array(len(word), False)

		global guessed_letters
		global failed_tries
		guessed_letters = []
		failed_tries = 0
		with run_algorithm(submission.source) as process:
			def guess(letter):
				global guessed_letters
				guessed_letters.append(letter)
				found_something = False

				for i in range(len(word)):
					if word[i] == letter:
						has_found_letter[i] = True
						found_something = True

				if not found_something:
					global failed_tries
					failed_tries += 1

			def get_word_letter(i):
				if has_found_letter[i]:
					return word[i]
				else:
					return -1

			process.procedures.play(len(word), callbacks=[guess, get_word_letter])

		# evaluator
		print(f"It guessed wrong {failed_tries} times", end=' ')
		if(failed_tries < 11):
			print("(correct)", end=' ')
		else:
			print("(too many)", end=' ')
			less_than_eleven_tries = False

		found_same_letter = False
		for i in range(len(guessed_letters)):
			for j in range(i+1, len(guessed_letters)):
				if guessed_letters[i] == guessed_letters[j]:
						found_same_letter = True
						break
			
			if found_same_letter:
				break

		if not found_same_letter:
			print("and did not guess the same letter twice")
		else:
			print("and guessed the same letters multiple times")
			guessed_each_letter_only_once = False

	evaluation.data(dict(goals=[dict(won_the_game=less_than_eleven_tries), dict(guess_only_once=guessed_each_letter_only_once)]))



def get_random_word(dictionary_directory):
	dictionary = open(dictionary_directory, "r")
	num_of_words = len(dictionary.read().strip().split("\n"))
	dictionary.close()

	dictionary = open(dictionary_directory, "r")
	word = dictionary.read().strip().split("\n")[random.randint(0, num_of_words-1)]
	dictionary.close()

	return word;

def turn_into_int_array(word):
	int_word = []

	for char in word:
		int_word.append(ord(char))

	return int_word

def fill_boolean_array(length, default):
	array = []
	
	for i in range(length):
		array.append(default)

	return array
	

main()
