
class Solution extends Skeleton {
	
	int squarei, squarej;
	int[] sudokuDim;
	int[][] sudoku;
	boolean[] forbiddenX, forbiddenY;



	void fill(int[][] grid) {
		int[][] sudoku = grid;

		int[][] square;
		forbiddenX = new boolean[sudoku.length];
		forbiddenY = new boolean[sudoku.length];
		
		while(!isFull()) {
			for(int num = 1; num <= sudoku.length; num++) {
				
				for(int i = 0; i < getNumberOfSquares(); i++) {
					getForbiddenIndexes(num);
					
					square = getSquare(i);
					
					if(!squareHasNumber(num, square))
						putNumberInSquare(num, square); // Puts it only if ONE check is free.
					
				}
				
				resetBooleanArray(forbiddenX);
				resetBooleanArray(forbiddenY);
				
			} // FINE FOR
		} // FINE WHILE
		
	}
	
	
	/* -------------------------------------------------------------------------------- */
	
	
	boolean isFull() {
		
		for(int[] row : sudoku) {
			for(int check : row) {
				if(check == 0) return false;
			}
		}
		
		return true;
		
	}
	
	void getForbiddenIndexes(int num) {
		
		for(int i = 0; i < sudoku.length; i++) {
			for(int j = 0; j < sudoku.length; j++) {
				
				if(sudoku[i][j] == num) {
					forbiddenX[j] = true;
					forbiddenY[i] = true;
				}
				
			}
		}
		
	}
	
	int getNumberOfSquares() {
		
		return (int) (Math.sqrt(sudoku.length) * Math.sqrt(sudoku[0].length));
		
	}
	
	int[][] getSquare(int numberOfSquare) {
		
		int[][] square = new int[(int) Math.sqrt(sudokuDim[1])][(int) Math.sqrt(sudokuDim[0])];
		
		squarei = 0;
		squarej = 0;
		for(int i = 0; i < numberOfSquare; i++) {
			
			squarej += square.length;
			if((i+1) % square[0].length == 0) {
				squarej = 0;
				squarei += square[0].length;
			}
			
		}
		
		for(int i = 0; i < square.length; i++) {
			for(int j = 0; j < square[i].length; j++) {

				square[i][j] = sudoku[squarei][squarej];
				squarej++;
				
			}
			squarej -= square[i].length;
			squarei++;
		}
		squarei -= square.length;
		
		return square;
		
	}
	
	boolean squareHasNumber(int num, int[][] square) {
		
		for(int[] row : square) {
			for(int check : row) {
				if(check == num) return true;
			}
		}
		
		return false;
		
	}
	
	void putNumberInSquare(int num, int[][] square) {
		
		int numi = -1, numj = -1;
		
		for(int i = 0; i < square.length; i++) {
			for(int j = 0; j < square[i].length; j++) {
				
				if(square[i][j] == 0 && numi != -1) return;
				else if (square[i][j] == 0 && !forbiddenY[squarei+i] && !forbiddenX[squarej+j]) {
					numi = i;
					numj = j;
				}
				
			}
		}
		
		sudoku[squarei+numi][squarej+numj] = num;
		
		
	}
	
	void resetBooleanArray(boolean[] toReset) {
		
		for(int i = 0; i < toReset.length; i++)
			toReset[i] = false;
		
	}

}
