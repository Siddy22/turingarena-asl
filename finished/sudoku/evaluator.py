from turingarena import *
import time

def main():
	all_passed = True

	cases = open("cases/cases.txt", "r").readlines()
	cases = [x.strip() for x in cases]

	solutions = open("cases/solutions.txt", "r").readlines()
	solutions = [x.strip() for x in solutions]

	for number_of_case in range(int(cases[0])):
		case = get_sudoku(number_of_case, cases)
		solution = get_sudoku(number_of_case, solutions)

	with run_algorithm(submission.source) as process:
		process.procedures.fill(case)

	if solution == case:
		print("si")
	else:
		print("no")
		all_passed = False

	cases.close()
	solutions.close()
	



def get_sudoku(number_of_case, sudoku_list):
	sudoku = []
	line_count = 2
	
	# Questo for passa al caso #number_of_case
	for i in range(number_of_case):
		line_count = int(sudoku_list[line_count].split('x')[1]) + 2
		# cases[line_count] ci trova la dimensione del sudoku (es "4x4")
		# .split('x') ci ritorna un'array con le dimensioni (es ["4", "4"]) e prendiamo la Y
		# +2 perchè, per passare alle dimensioni del caso successivo, dobbiamo saltare l'a capo e passare alle dimensioni del sudoku successivo

	for i in range(int(sudoku_list[line_count].split("x")[1])):
		sudoku.append(sudoku_list[line_count + i + 1].split(' '))
		sudoku[i] = [int(sudoku[i][j]) for j in range(len(sudoku[i]))]

	return sudoku


main()

















