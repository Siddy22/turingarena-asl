function get_number_of_divisors(num);
procedure get_divisors(num);
function get_divisor(i);

main {

	read num;

	call n = get_number_of_divisors(num);
	write n;	

	call get_divisors(num);

	for i to n {

		call ans = get_divisor(i);
		write ans;

	}

}
