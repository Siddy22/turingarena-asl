import random

from turingarena import *

all_passed = True
for _ in range(10):
	num = random.randint(0, 1000)
	
	try:
		with run_algorithm(submission.source) as process:
			even = process.functions.odd_or_even(num)
			odd = process.functions.odd_or_even(num)

	except AlgorithmError as e:
		print(num, "--->", even)
		all_passed = False

	if num%2 == even:
		print(num, "--->", even, "(correct)")
	else:
		print(num, "--->", even, "(wrong)")
		all_passed = False

evaluation.data(dict(goals=dict(correct=all_passed)))
