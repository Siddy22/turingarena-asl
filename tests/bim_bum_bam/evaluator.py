from turingarena import *

n_moves = 10
current_player = 0

try:
	with run_algorithm(submission.player1) as p1, run_algorithm(submission.player2) as p2:
		players = (p1, p2)
		victories = [0, 0]

		for i, p in enumerate(players):
			current_player = i
			p.procedures.start(n_moves)

		print("\nPlayer 1 is EVEN, Player 2 is ODD")
		for i in range(n_moves):
			plays = [None, None]
			for j, p in enumerate(players):
				current_player = j
				plays[j] = p.functions.play()

			print(f"\tPlayer 1 threw {plays[0]}, Player 2 threw {plays[1]}. Player {(plays[0]+plays[1])%2+1} won!")
			victories[(plays[0]+plays[1])%2] += 1

			for j, p in enumerate(players):
				current_player = j
				p.procedures.done(i, plays[j], plays[1-j])

		print(f"\nFinal score: {victories[0]} - {victories[1]}.", end = ' ')
		if victories[0] > victories[1]:
			print("Player 1 wins!")
		elif victories[0] == victories[1]:
			print("It's a draw!")
		else:
			print("Player 2 wins!")

except Exception as e:
	print(f"Player {current_player+1} LOST because he encountered an error during runtime:\n\n{e}")
