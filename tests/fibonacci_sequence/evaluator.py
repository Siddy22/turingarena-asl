import random
from turingarena import *

def correct_fibonacci_sequence(n):
	fibonacci_sequence = [1, 1]

	for i in range(2, n):
		fibonacci_sequence.append(fibonacci_sequence[i-2] + fibonacci_sequence[i-1])

	return fibonacci_sequence[:n]

all_correct = True
for _ in range(10):

	if _ == 0:
		n = 0
	elif _ == 1:
		n = 1
	else:
		n = random.randint(2, 46)

	try:
		with run_algorithm(submission.source) as process:
			process.procedures.get_fibonacci_sequence(n)
			fibonacci_sequence = [process.functions.get_fibonacci_element(i) for i in range(n)]

	except AlgorithmError as e:
		all_correct = False
		pass

	if fibonacci_sequence == correct_fibonacci_sequence(n):
		print("(correct)", end=' ')
	else:
		print("(wrong)", end=' ')
		all_correct = False
	print(fibonacci_sequence)

evaluation.data(dict(goals=dict(correct=all_correct)))


