int **MATRIX;

int dupe(int num) {
	return num+1;
}

int get_element(int i, int j) {
	return MATRIX[i][j];
}

int len(int **matrix) {
	if(sizeof(matrix) != 0)
		return sizeof(matrix)/sizeof(matrix[0]);
	else
		return 0;
}

int len(int *array) {
	if(sizeof(array) != 0)
		return sizeof(array)/sizeof(array[0]);
	else
		return 0;
}

void invert(int **matrix) {

	for(int i = 0; i < len(matrix); i++) {
		int inverted_i = len(matrix) - i - 1;

		for(int j = 0; j < len(matrix[i]); j++) {
			int inverted_j = len(matrix[i]) - j - 1;
			
			int temp = matrix[i][j];
			matrix[i][j] = matrix[inverted_i][inverted_j];
			matrix[inverted_i][inverted_j] = temp;

		}
	}

	MATRIX = matrix;

}
