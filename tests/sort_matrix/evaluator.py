import random
from turingarena import *

def sort_matrix(matrix_to_sort):
	b = []
	for i in range(len(matrix_to_sort)):
		b.append(sorted(matrix_to_sort[i]))
	return sorted(b)

all_passed = True
for _ in range(10):
    n, m = 4, 4
    a = [[random.randint(0, 9) for j in range(m)] for i in range(n)]
    b = []
    try:
        with run_algorithm(submission.source) as process:
            process.procedures.sort(n, m, a)
            b = [[process.functions.get_element(i, j) for j in range(m)] for i in range(n)]
    except AlgorithmError as e:
        print(e)
        all_passed = False
    if b == sort_matrix(a):
        print("correct!")
    else:
        print("WRONG!")
        all_passed = False

evaluation.data(dict(goals=dict(correct=all_passed)))
