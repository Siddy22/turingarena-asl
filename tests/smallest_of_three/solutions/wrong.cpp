
int smallest_of_three(int a, int b, int c) {
    
 	int smallest = a;
	if(smallest < b) smallest = b;
	if(smallest > c) smallest = c;

	return smallest;

}
