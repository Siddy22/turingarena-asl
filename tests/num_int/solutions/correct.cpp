
int get_last_digit(int num) {

	return num % 10;

}

int get_first_digit(int num) {
    
	while(num >= 10) num /= 10;

	return num;

}

int get_number_of_digits(int num) {
    
	int num_digits = 1;

	while(num >= 10) {

		num /= 10;
		num_digits++;

	}

	return num_digits;

}

#include <cmath>
int get_opposite_number(int num) {
    
	int opposite = 0;

	for(int i = 0; i < get_number_of_digits(num); i++) {

		opposite *= 10;
		opposite += (int) (num / pow(10, i)) % 10;

	}

	return opposite;

}

int get_most_valuable_digit(int num) {

	int max_digit = 0;

	for(int i = 0; i < get_number_of_digits(num); i++) {

		int digit = (int) (num / pow(10, i)) % 10;
		if(digit > max_digit) max_digit = digit;

	}

	return max_digit;

}

